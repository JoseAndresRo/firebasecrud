package com.example.firebasecrud.model;

/**
 * Created by Andrés Rodríguez on 19/10/2020.
 */
public class Person {
    private final String id;
    private final String name;
    private final String secondName;
    private final String email;
    private final String password;

    public Person(String id, String name, String secondName, String email, String password) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
