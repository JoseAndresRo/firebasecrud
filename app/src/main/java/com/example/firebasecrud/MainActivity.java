package com.example.firebasecrud;

import android.app.ActivityManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


public class MainActivity extends AppCompatActivity {
    private AppBarConfiguration mAppBarConfiguration;

    NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setBackgroundResource(R.color.colorYellow);
        navigationView.setItemTextColor(ColorStateList.valueOf(getColor(R.color.colorWhite)));

        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.welcomeFragment,R.id.userAddFragment, R.id.editUsersFragment, R.id.deleteUsersFragment)
                .setOpenableLayout(drawerLayout)
                .build();
        navController = Navigation.findNavController(this, R.id.navHostFragment);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                manageDestination(destination);

            }
        });

        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private void manageDestination(NavDestination destination) {
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(MainActivity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null){
            if (this.getCurrentFocus() != null){
                inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }




    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        NavDestination navDestination = navController.getCurrentDestination();
        if (navDestination != null) {
            if (navDestination.getId() == R.id.userAddFragment) {
                menu.findItem(R.id.iconAddUSer).setEnabled(true);
                menu.findItem(R.id.iconAddUSer).setVisible(true);
                menu.findItem(R.id.iconDeleteUSer).setEnabled(false);
                menu.findItem(R.id.iconDeleteUSer).setVisible(false);
                menu.findItem(R.id.iconEditUSer).setEnabled(false);
                menu.findItem(R.id.iconEditUSer).setVisible(false);
            } else if (navDestination.getId() == R.id.editUsersFragment) {
                menu.findItem(R.id.iconEditUSer).setEnabled(true);
                menu.findItem(R.id.iconEditUSer).setVisible(true);
                menu.findItem(R.id.iconAddUSer).setEnabled(false);
                menu.findItem(R.id.iconAddUSer).setVisible(false);
                menu.findItem(R.id.iconDeleteUSer).setEnabled(false);
                menu.findItem(R.id.iconDeleteUSer).setVisible(false);
            } else if (navDestination.getId() == R.id.deleteUsersFragment) {
                menu.findItem(R.id.iconDeleteUSer).setEnabled(true);
                menu.findItem(R.id.iconDeleteUSer).setVisible(true);
                menu.findItem(R.id.iconAddUSer).setEnabled(false);
                menu.findItem(R.id.iconAddUSer).setVisible(false);
                menu.findItem(R.id.iconEditUSer).setEnabled(false);
                menu.findItem(R.id.iconEditUSer).setVisible(false);
            } else if (navDestination.getId() == R.id.welcomeFragment) {
                menu.findItem(R.id.iconAddUSer).setEnabled(false);
                menu.findItem(R.id.iconAddUSer).setVisible(false);
                menu.findItem(R.id.iconDeleteUSer).setEnabled(false);
                menu.findItem(R.id.iconDeleteUSer).setVisible(false);
                menu.findItem(R.id.iconEditUSer).setEnabled(false);
                menu.findItem(R.id.iconEditUSer).setVisible(false);
            }
            else {
                throw new RuntimeException("Unknown Nav: " + navDestination.getLabel());
            }
        }
        return true;
    }

}
