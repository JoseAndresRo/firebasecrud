package com.example.firebasecrud.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.firebasecrud.R;
import com.example.firebasecrud.databinding.FragmentUserAddBinding;
import com.example.firebasecrud.model.Person;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

/**
 * created by Andres Rodriguez
 */
public class UserAddFragment extends Fragment {
   private FragmentUserAddBinding binding;
    private DatabaseReference databaseReference;

    private String name;
    private String secondName;
    private String email;
    private String password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        
        startFirebase();
    }

    private void startFirebase() {
        FirebaseApp.initializeApp(getContext());
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentUserAddBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String id = UUID.randomUUID().toString();
        name = String.valueOf(binding.etName.getText());
        secondName = String.valueOf(binding.etSecondName.getText());
        email = String.valueOf(binding.etEMail.getText());
        password = String.valueOf(binding.etPassword.getText());


        if (item.getItemId() == R.id.iconAddUSer) {
            if (validateFields()) {
                Person person = new Person(id, name, secondName, email, password);
                databaseReference.child("tablePerson").child(person.getId()).setValue(person);
                Toast.makeText(getContext(), "User was Added!", Toast.LENGTH_SHORT).show();
                clearFields();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean validateFields() {
        if (name.isEmpty()) {
            binding.etName.setError("Required");
            binding.etName.setFocusable(true);
            return false;
        } else if (secondName.isEmpty()) {
            binding.etSecondName.setError("Required");
            binding.etSecondName.setFocusable(true);
            return false;
        } else if (email.isEmpty()) {
            binding.etEMail.setError("Required");
            binding.etEMail.setFocusable(true);
            return false;
        } else if (password.isEmpty()) {
            binding.etPassword.setError("Required");
            binding.etPassword.setFocusable(true);
            return false;
        }
        return true;
    }

    private void clearFields() {
        binding.etName.setText("");
        binding.etSecondName.setText("");
        binding.etEMail.setText("");
        binding.etPassword.setText("");
    }
}